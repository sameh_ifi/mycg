﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.SegmentIntersection
{
    class Line1:Algorithm
    {

        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                for (int j = 0; j < lines.Count; j++)
                {
                    if (j == i)
                        continue;
                    double a1, b1,a2,b2;
                    a1 = (lines[j].End.Y - lines[j].Start.Y) / (lines[j].End.X - lines[j].Start.X);
                    b1 = lines[j].Start.Y - (a1 * lines[j].Start.X);
                    a2 = (lines[i].End.Y - lines[i].Start.Y) / (lines[i].End.X - lines[i].Start.X);
                    b2 = lines[i].Start.Y - (a2 * lines[i].Start.X);
                    double X = (b2 - b1) / (a1 - a2);
                    double Y = (X * a1) + b1;
                    Point P = new Point(X,Y);
                    if (HelperMethods.PointOnSegment(P, lines[j].Start, lines[j].End) == true)
                        outPoints.Add(P);


                }
            }
        }

        public override string ToString()
        {
            return "Line1";
        }

    }
}
