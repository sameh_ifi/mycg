﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class GrahamScan : Algorithm
    {
        Point p1;
        Point p2;
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            double my = 100000;
            int PointIdx = 0;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].Y < my)
                {
                    my = points[i].Y;
                    PointIdx = i;
                }
            }

            p1 = points[PointIdx];
            p2 = new Point(points[PointIdx].X + 5, points[PointIdx].Y);

            List<Point> newPoint = new List<Point>();
            newPoint = points;
            newPoint.Remove(p1);
            newPoint.Sort(com);
            Stack<Point> stk = new Stack<Point>();
            stk.Push(p1);
            p2 = newPoint[0];
            stk.Push(p2);
            Point p3 = new Point(0, 0);
            int idx = 1;
            while (idx < newPoint.Count)
            {
                Line ml = new Line(p1, p2);
                p3 = newPoint[idx];
                if (HelperMethods.CheckTurn(ml, p3) == Enums.TurnType.Left)
                {
                    stk.Push(p3);
                    p1 = p2;
                    p2 = p3;
                    idx++;
                }
                else
                {
                    stk.Pop();
                    p2 = stk.Pop();
                    p1 = stk.Peek();
                    stk.Push(p2);
                }

            }
            outPoints.AddRange(stk.ToArray());


        }

        public override string ToString()
        {
            return "Convex Hull - Graham Scan";
        }

        public int com(Point x, Point y)
        {
            Point v1 = p1.Vector(p2);
            Point v2 = p1.Vector(x);
            double angle1 = HelperMethods.GetAngle(v1, v2);

            Point v11 = p1.Vector(p2);
            Point v22 = p1.Vector(y);
            double angle2 = HelperMethods.GetAngle(v11, v22);
            if (angle1 < angle2)
                return -1;
            else if (angle1 == angle2)
                return 0;
            else
                return 1;


        }
    }
}
