﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremePoints : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            // I have made a modification in this file..  
            for (int i = 0; i < points.Count(); i++)
            {
                bool ok=true;
                for (int j = 0; j <  points.Count(); j++)
                {
                    if (j == i)
                        continue;
                    if (ok == false)
                        break;
                    for (int k = 0; k <  points.Count(); k++)
                    {
                        if (k == i)
                            continue;
                        if (ok == false)
                            break;
                        for (int l = 0; l <  points.Count(); l++)
                        {
                            if (l == i)
                                continue;
                            if (HelperMethods.PointInTriangle(points[i], points[j], points[k], points[l]) == CGUtilities.Enums.PointInPolygon.Inside)
                            {
                                ok = false;
                                break;
                            }
                        }
                    }
                }
                if (ok == true)
                    outPoints.Add(points[i]);
            }
        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Points";
        }
    }
}
