﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities.DataStructures;
namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class Incremental : Algorithm
    {
        Point Center;
        Point NB;

        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            Center = new Point(0, 0);
            Center.X = (points[0].X + points[1].X + points[2].X) / 3;
            Center.Y = (points[0].Y + points[1].Y + points[2].Y) / 3;
            NB = new Point(Center.X + 5, Center.Y);
            OrderedSet<Point> Ch = new OrderedSet<Point>(new Comparison<Point>(com));
            Ch.Add(points[0]);
            Ch.Add(points[1]);
            Ch.Add(points[2]);
            for (int i = 3; i < points.Count; i++)
            {
                Point p = points[i];
                KeyValuePair<Point, Point> mykey = new KeyValuePair<Point, Point>();
                mykey = Ch.DirectRightAndLeftRotational(p);
                Point pre = new Point(0, 0);
                pre=mykey.Value;
                Point next = new Point(0, 0);
                next=mykey.Key;
                if (pre == next)
                    continue;
                Line l = new Line(pre,next);
                if(HelperMethods.CheckTurn(l,p)== Enums.TurnType.Right) // outside poly 
                {
                    // Get Left Supporting Line
                    KeyValuePair<Point, Point> key2 = new KeyValuePair<Point, Point>();
                    key2 = Ch.DirectRightAndLeftRotational(pre);
                    Point newPre = key2.Value;
                    while ( HelperMethods.CheckTurn(new Line(p,pre),newPre)!= Enums.TurnType.Right)
                    {
                        Ch.Remove(pre);
                        pre = newPre;
                        newPre = Ch.DirectRightAndLeftRotational(pre).Value;
                    }

                    // Get Right Supporting Line

                    key2 = Ch.DirectRightAndLeftRotational(next);
                    Point newNext = key2.Key;
                    while (HelperMethods.CheckTurn(new Line(p,next), newNext) != Enums.TurnType.Left)
                    {
                        Ch.Remove(next);
                        next = newNext;
                        newNext = Ch.DirectRightAndLeftRotational(next).Key;
                    }
                    Ch.Add(p);
                }
                else if(HelperMethods.CheckTurn(l,p)== Enums.TurnType.Colinear)
                {
                    if(HelperMethods.PointOnSegment(p,pre,next)== false)
                    {
                        Point v1 = pre.Vector(next);
                        Point v2 = pre.Vector(p);
                        double angle = HelperMethods.GetAngle(v1, v2);
                        if (angle == 180)
                            Ch.Remove(pre);
                        if (angle == 0)
                            Ch.Remove(next);
                        Ch.Add(p);
                    }
                   
                }
            }
            outPoints.AddRange(Ch.ToList());
           
        }

        public override string ToString()
        {
            return "Convex Hull - Incremental";
        }

        public int com(Point x, Point y)
        {
            Point v1 = Center.Vector(NB);
            Point v2 = Center.Vector(x);
            double angle1 = HelperMethods.GetAngle(v1, v2);

            Point v11 = Center.Vector(NB);
            Point v22 = Center.Vector(y);
            double angle2 = HelperMethods.GetAngle(v11, v22);
            if (angle1 < angle2)
                return -1;
            else if (angle1 == angle2)
                return 0;
            else
                return 1;

        }
       
    }
}
