﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class QuickHull : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            // 1. get minx , miny
            double MinX = 10000, MaxX = -100000;
            Point Xmin=new Point(0,0);
             Point Xmax=new Point(0,0);
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].X > MaxX)
                { 
                    MaxX = points[i].X;
                    Xmax=points[i];
                }

                if (points[i].X < MinX)
                {  MinX = points[i].X;
                    Xmin=points[i];
                }
            }
            List<Point> Left = new List<Point>();
            List<Point> Right = new List<Point>();
             Line myline=new Line(Xmin,Xmax);
            for (int i = 0; i < points.Count; i++)
            {

                if (HelperMethods.CheckTurn(myline, points[i]) == Enums.TurnType.Left)
                    Left.Add(points[i]);

                if (HelperMethods.CheckTurn(myline, points[i]) == Enums.TurnType.Right)
                    Right.Add(points[i]);
            }
            List<Point> T = new List<Point>();
            List<Point> D = new List<Point>();
            T = QH(Xmin, Xmax, Left);
            D = QH(Xmax, Xmin, Right);
            outPoints.Add(Xmin);
            outPoints.AddRange(T);
            outPoints.Add(Xmax);
            outPoints.AddRange(D);
        }
        public List<Point> QH(Point x1,Point x2,List<Point> L)
        {
            if (L.Count == 0)
                return L;
            double d = -1;
            Point mxpoint = new Point(0, 0);

            for (int i = 0; i < L.Count; i++)
            {
                Point v1 = new Point(0,0);
                Point v2 = new Point(0, 0);
                v1 = x1.Vector(x2);
                v2 = x1.Vector(L[i]);
                double cross = HelperMethods.CrossProduct(v1, v2);
                double distance = Math.Sqrt( ((x2.X - x1.X) * (x2.X - x1.X) ) + ( (x2.Y - x1.Y) *(x2.Y - x1.Y) ));
                double h = cross / distance;
                if (h > d)
                {
                    d = h;
                    mxpoint = L[i];
                }
            }
            Line line1 = new Line(x1, mxpoint);
            Line line2 = new Line(mxpoint, x2);
            List<Point> L1 = new List<Point>();
            List<Point> L2 = new List<Point>();
            for (int i = 0; i < L.Count; i++)
            {
                if (HelperMethods.CheckTurn(line1, L[i]) == Enums.TurnType.Left)
                    L1.Add(L[i]);
                if (HelperMethods.CheckTurn(line2, L[i]) == Enums.TurnType.Left)
                    L2.Add(L[i]);
            }
            List<Point> ret1 = new List<Point>();
            List<Point> ret2 = new List<Point>();
            ret1 = QH(x1, mxpoint, L1);
            ret2 = QH(mxpoint, x2, L2);
            ret1.Add(mxpoint);
            ret1.AddRange(ret2);
            return ret1;

        }

        public override string ToString()
        {
            return "Convex Hull - Quick Hull";
        }
    }
}
