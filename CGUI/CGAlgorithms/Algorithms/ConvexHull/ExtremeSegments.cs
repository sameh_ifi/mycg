﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremeSegments : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
           
            for (int i = 0; i <points.Count() ; i++)
            {
                for (int j = 0; j < points.Count(); j++)
                {
                    if (j == i)
                       continue;
                     bool ext = true;
                     Line myline = new Line(points[i], points[j]);
                    for (int k = 0; k < points.Count(); k++)
                    {
                        if (k == j || k == i)
                            continue;
                        if (HelperMethods.CheckTurn(myline, points[k]) == Enums.TurnType.Right || HelperMethods.CheckTurn(myline, points[k]) == Enums.TurnType.Colinear)
                        {
                            ext = false;
                            break;
                        }
                       
                    }
                    if (ext == true)
                    {
                        outPoints.Add(points[i]);
                        outLines.Add(myline);
                    }

                }
            }
        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Segments";
        }
    }
}
