﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class JarvisMarch : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            Point Lnode = new Point(0, 0);
            double my = 100000;
            int PointIdx = 0;
            for (int i = 0; i < points.Count; i++)
            {
                if(points[i].Y<my)
                {
                    my = points[i].Y;
                    PointIdx = i;
                }
            }

            Point p1 = points[PointIdx];
            Point p2 = new Point(points[PointIdx].X + 5, points[PointIdx].Y);
            Line myline = new Line(p1, p2);

            outPoints.Add(p1);
            while(true)
            {
                double min_angle = 1000;
                int min_k = 100000;

                for (int k = 0; k < points.Count; k++)
                {
                    if (points[k] == p1 || points[k] == p2)
                        continue;
                    Point v1 = p1.Vector(p2);
                    Point v2 = p1.Vector(points[k]);
                    double angle = HelperMethods.GetAngle(v1, v2);
                    if(angle<min_angle)
                    {
                        min_angle = angle;
                        min_k = k;
                    }

                }
                p2 = p1;
                p1 = points[min_k];
                if (points[min_k] == points[PointIdx])
                    break;

                outPoints.Add(points[min_k]);


            }




        }

        public override string ToString()
        {
            return "Convex Hull - Jarvis March";
        }
    }
}
