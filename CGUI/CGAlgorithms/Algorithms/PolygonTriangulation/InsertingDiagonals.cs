﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class InsertingDiagonals : Algorithm
    {
        List<Line> outputDiagonals=new List<Line>();
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
           
            int c_idx = getMinX(polygons[0].lines);
            Point c = new Point(polygons[0].lines[c_idx].Start.X, polygons[0].lines[c_idx].Start.Y);
            int c_prev_idx, c_next_idx;
            KeyValuePair<int, int> pre_next = getPrev_next_idx(c_idx, polygons[0].lines.Count);
            c_prev_idx = pre_next.Key;
            c_next_idx = pre_next.Value;
            Point c_prev = new Point(polygons[0].lines[c_prev_idx].Start.X, polygons[0].lines[c_prev_idx].Start.Y);
            Point c_next = new Point(polygons[0].lines[c_next_idx].Start.X, polygons[0].lines[c_next_idx].Start.Y);
            Line l = new Line(c_prev, c);
            if (HelperMethods.CheckTurn(l, c_next) == Enums.TurnType.Right)
            {
                polygons[0].lines.Reverse();
                for (int i = 0; i < polygons[0].lines.Count; i++)
                {
                    Point tem = polygons[0].lines[i].Start;
                    polygons[0].lines[i].Start = polygons[0].lines[i].End;
                    polygons[0].lines[i].End = tem;
                }
            }

            List<Point> poly = new List<Point>();
            for (int i = 0; i < polygons[0].lines.Count; i++)
                poly.Add(polygons[0].lines[i].Start);
            insertDiagonal(poly);
            outLines.AddRange(outputDiagonals);
            outputDiagonals.Clear();


        }

        public override string ToString()
        {
            return "Inserting Diagonals";
        }


        public void insertDiagonal(List<Point> poly)
        {
            if(poly.Count > 3 )
            {
                Point c = new Point(0,0);
                KeyValuePair<int, int> mykey=new KeyValuePair<int,int>();
                for (int i = 0; i < poly.Count; i++)
                {
                    mykey = getPrev_next_idx(i, poly.Count);
                    Line myline = new Line(poly[mykey.Key], poly[i]);
                    if(HelperMethods.CheckTurn(myline,poly[mykey.Value])== Enums.TurnType.Left)
                    {
                        c = poly[i];
                        break;
                    }
                }
                Point MaxDistantPoint=null;
                int MaxDistantPointIdx = 0;
                double d = -10000000;
                Point x1 = new Point(0, 0);
                Point x2 = new Point(0, 0);
                x1 = poly[mykey.Key]; // prev point 
                x2 = poly[mykey.Value]; // next  point 
                for (int i = 0; i < poly.Count; i++)
                {
                    
                    if(HelperMethods.PointInTriangle(poly[i],x1,c,x2) == Enums.PointInPolygon.Inside)
                    {
                        Point v1 = new Point(0, 0);
                        Point v2 = new Point(0, 0);
                        v1 = x2.Vector(x1);
                        v2 = x1.Vector(poly[i]);
                        double cross = HelperMethods.CrossProduct(v1, v2);
                        double distance = Math.Sqrt(((x2.X - x1.X) * (x2.X - x1.X)) + ((x2.Y - x1.Y) * (x2.Y - x1.Y)));
                        double h = cross / distance;
                        if (  h > d)
                        {
                            d =  h;
                            MaxDistantPoint = poly[i];
                            MaxDistantPointIdx = i;
                        }

                    }
                   
                }
                if (MaxDistantPoint == null)
                    outputDiagonals.Add(new Line(x1, x2));
                else
                    outputDiagonals.Add(new Line(c, MaxDistantPoint));
                List<Point> p1 = new List<Point>();
                List<Point> p2 = new List<Point>();
                if(MaxDistantPoint==null)
                {
                    p1.AddRange(poly);
                    p1.Remove(c);
                    p2.Add(x1);
                    p2.Add(c);
                    p2.Add(x2);
                }
                else
                {
                    int idx_c=poly.IndexOf(c);
                    int i = idx_c;
                    p1.Add(c);
                    i++;
                    while (i != MaxDistantPointIdx)
                    {
                        if (i > poly.Count - 1)
                        {
                            i = 0;
                            continue;
                        }
                        p1.Add(poly[i]);
                        i++;
                    }
                    p1.Add(poly[MaxDistantPointIdx]);

                     i = MaxDistantPointIdx;
                    p2.Add(MaxDistantPoint);
                    i++;
                    while (i != idx_c)
                    {
                        if (i > poly.Count - 1)
                        { i = 0;
                        continue;
                        }
                        p2.Add(poly[i]);
                        i++;
                    }
                    p2.Add(poly[idx_c]);

                }

                insertDiagonal(p1);
                insertDiagonal(p2);


            }
        }
        public int getMinX(List<Line> poly)
        {
            double x = poly[0].Start.X;
            int idx = 0;
            for (int i = 0; i < poly.Count; i++)
            {
                if (poly[i].Start.X < x)
                {
                    x = poly[i].Start.X;
                    idx = i;
                }
            }
            return idx;
        }
        public KeyValuePair<int, int> getPrev_next_idx(int idx, int list_size)
        {
            int c_prev_idx = idx - 1;
            if (c_prev_idx < 0)
                c_prev_idx = list_size - 1;
            int c_next_idx = idx + 1;
            if (c_next_idx > list_size - 1)
                c_next_idx = 0;
            KeyValuePair<int, int> res = new KeyValuePair<int, int>(c_prev_idx, c_next_idx);
            return res;
        }

    }
}
